<?php

use App\Models\Usuario;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class TestCase extends Illuminate\Foundation\Testing\TestCase {

	use DatabaseMigrations;

	/**
	 * The base URL to use while testing the application.
	 *
	 * @var string
	 */
	protected $baseUrl; #'http://fiscalsherlock:90';
	protected $token = "";
	
	/**
	 * @var int
	 */
	protected $userId = 3;

	public function setUp() {
		parent::setUp();

//		putenv('DB_DATABASE=fiscalsherlock-test');
//		putenv('DB_HOST=127.0.0.1');

		$testURL = getenv('TEST_URL');
		if (isset($testURL)) {
			$this->baseUrl = getenv('TEST_URL');
		}
	}

	public function tearDown() {
		//Artisan::call('migrate:reset');
		parent::tearDown();
	}

	/**
	 * Creates the application.
	 *
	 * @return \Illuminate\Foundation\Application
	 */
	public function createApplication() {
		$app = require __DIR__ . '/../bootstrap/app.php';

		if (file_exists(dirname(__DIR__) . '/.env.testing')) {
			Dotenv::load(dirname(__DIR__), '.env.testing');
		}

		$app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();
		return $app;
	}

	protected function callRoute($method, $route, $data = [], $headerToken = true) {
		$url = $this->prepareUrlForRequest($this->baseUrl . "/testing/" . $route);
		//$url = "testing/".$route;

		$server = [];
		if ($headerToken) {
//			$this->debug("Rota com TOKEN (".$this->token.")");
			if (empty($this->token)) {
				$this->login();
			}
			$server['HTTP_Authorization'] = 'Bearer ' . $this->token;
		}

		$server['Content-Type'] = 'application/json';

		$this->refreshApplication();
		switch ($method) {
			case 'GET':
				$return = $this->call('GET', $url, $data, [], [], $server);
				//dd("NETO ".$method);
				//		$return = $this->get($url, $server);
				break;

			case 'POST':
				$return = $this->call('POST', $url, $data, [], [], $server);
				break;

			case 'PUT':
				$return = $this->call('PUT', $url, $data, [], [], $server);
				//$return = $this->put($url, $data, $server);
				break;

			case 'DELETE':
				$return = $this->call('DELETE', $url, $data, [], [], $server);
				//$return = $this->delete($url, $data, $server);
				break;

			default:
				throw new Exception("Methodo não encontrado", 1);
				break;
		}
		return $return;
	}

	protected function login($userId = null) {
		
		if (!empty($userId)) {
			$this->userId = $userId;
		}
		
		if (empty($this->token)) {
			$user = Usuario::find($this->userId);
			$this->token = JWTAuth::fromUser($user);
		}
		return $this->token;
	}

	protected function debug($text) {
		$debug = getenv('APP_DEBUG');
		if ($debug == 'true') {
//			echo "\e[34m$text\e[0m";
		}
	}

	protected function carregaMock($file) {
		$fileFull = dirname(__DIR__) . '/tests/mocks/' . $file;
		$fileContent = file_get_contents($fileFull);

		$fileContent = str_replace('$$TEST$$', Date("Ymd-His"), $fileContent);

		$temp = json_decode($fileContent, true);
		//var_dump($temp); die();
		return $temp;
	}

}
