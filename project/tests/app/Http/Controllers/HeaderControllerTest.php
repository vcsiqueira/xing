<?php
namespace App\Http\Controllers;

class HeaderControllerTest extends \TestCase {

	public function testRouteHeaderShowComplete() {

//		$this->markTestIncomplete('Precisa testar a estrutura');

		$this->login();
		$response = $this->callRoute('GET', 'header/show/complete/200');
		$this->assertResponseOK();
	}

	public function setUp() {
		parent::setUp();
	}

	public function tearDown() {
		parent::tearDown();
		\Mockery::close();
	}

}
