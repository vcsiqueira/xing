<?php

namespace App\Http\Controllers;

class FormularioControllerTest extends \TestCase {

	/**
	 * A basic test example.
	 *
	 * @return void
	 */
	public function testSave() {
		$this->login();
		$mock = $this->carregaMock('formulario.save.json');
		$response = $this->callRoute('POST', 'formulario/save', $mock, true);
		$this->assertResponseOk();
	}

	public function testRouteFormularioShowComplete() {
//		$this->withoutMiddleware();
		$login = $this->login();
		$response = $this->callRoute('GET', 'formulario/show/complete/1');
		$this->assertResponseOk();
	}

}
