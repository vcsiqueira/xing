<?php

namespace App\Http\Controllers;

class UsuarioControllerTest extends \TestCase {

	public function testRoutePostLogin() {
		$login = [
			"username" => "user1",
			"password" => "123"
		];

		$response = $this->callRoute('POST', 'login', $login, false);
		$this->assertResponseStatus(200);
	}

	public function testRouteGetIndexNotParseToken() {
		$response = $this->callRoute('GET', "usuario", [], false);
		$this->assertResponseStatus(401);
	}

	public function testRouteGetIndex() {
		$this->login();
		$response = $this->callRoute('GET', "usuario/");
		$this->assertResponseOk();
	}

	public function testRouteGetShow() {
		$this->login();
		$response = $this->callRoute('GET', "usuario/" . $this->userId);
		$this->assertResponseOk();
	}

}
