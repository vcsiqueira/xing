<?php

namespace App\Http\Controllers;

class MobileControllerTest extends \TestCase {

	/**
	 * A basic test example.
	 *
	 * @return void
	 */
	public function testRouteMobileEstrutura() {
		$this->callRoute('POST', 'mobile/estrutura');
		$this->assertResponseOK();
	}

	public function testRouteHeaderSave() {
//		$this->markTestSkipped();
		$this->markTestIncomplete();
//		$data = {
//			"":""
//		};
		$data = [];
		$this->callRoute('POST', 'mobile/estrutura', $data);
		$this->assertResponseOK();
	}

}
