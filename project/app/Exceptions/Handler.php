<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\Facades\JWTAuth;

use Tymon\JWTAuth\Exceptions\JWTException;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Database\QueryException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Exception;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Illuminate\Http\Response;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
	\Symfony\Component\HttpKernel\Exception\HttpException::class,
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception  $e
	 * @return void
	 */
	public function report(Exception $e)
	{
		return parent::report($e);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Exception  $e
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e)
	{
		$type = get_class($e);
		$message = "".$e->getMessage();
		$code = $e->getCode();
		$info = "";
		switch (true) {
			case $e instanceof NotFoundHttpException:
				$code = 404;
				$message = "Resource not found: ".$request->getRequestUri();
				break;

			case $e instanceof QueryException:
				$code = 500;
				$info = array('sql'=>$e->getSql(), 'parameters' => $e->getBindings());
				break;
					
			case $e instanceof MethodNotAllowedHttpException:
				$code = 500;
				$message = "Method not allowed";
				$info = $e->getHeaders();
				$info['statusCode'] = $e->getStatusCode();
				break;
					
			case $e instanceof TokenInvalidException:
				$code = 401;
				$message = "Token is invalid:";
				break;
					
			case $e instanceof TokenExpiredException:
				$code = 401;
				$message = 'Token has expired';
// 				$info['new_token'] = JWTAuth::refresh(JWTAuth::getToken());
				break;
			case $e instanceof HttpException:
				if ($e->getStatusCode() == 401) {
					$code = 403;
					$message = "User not authorized to access this area.";
					$info['msg_origin'] = $e->getMessage();
				}
				break;
			case $e instanceof JWTException:
// 				if (empty($code)) {
// 					$code = (empty($e->getStatusCode())) ? 401 : $e->getStatusCode();
// 				}
				$code = 401;
				$message = $e->getMessage();
				$info = $request->server('HTTP_AUTHORIZATION');
				break;

			case $e instanceof \HttpException:
				if ($e->getCode() == 401) {
					$code = 401;
					$message = "User authorized, refresh token.";
					$info['new_token'] = $e->getMessage();
				}
				break;

			default:
				//     			return parents::render($request, $e);
				break;
		}
			
        $retorno = array(
				'response' => array(
						'responsecode' => $code,
						'typeError' => $type,
						'message' => $message,
                        'trace' => (getenv("APP_DEBUG") == "true") ?  $e->getTrace() : ''
				)
		);
		if (!empty($info)) {
			$retorno['response']['info'] = $info;
		}

		try {
			$response = new Response($retorno, $code);
		} catch (Exception $e) {
			// 			die($e->getMessage());
			$response = new Response($retorno, 500);
		}
		return $response->header('Content-Type', 'application/json');
	}
}
