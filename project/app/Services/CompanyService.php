<?php

namespace App\Services;

use App\Models\Company;
use App\Models\Rating;
use App\Models\Revew;
use App\Repositories\CompanyRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CompanyService {
    protected $repository;

    /**
     * UsuarioService constructor.
     */
    public function __construct(CompanyRepository $repository) {
        $this->repository = $repository;
    }

    public function load() {

        $json = Storage::disk('local')->get('data.json');
        $data = json_decode($json,true);

        DB::beginTransaction();
        try {
            //companies
            foreach ($data["companies"] as $companyLoop){
                $company = new Company();
                $company->name = $companyLoop["name"];
                $company->slug = $companyLoop["slug"];
                $company->city = $companyLoop["city"];
                $company->country = $companyLoop["country"];
                $company->industry = $companyLoop["industry"];

                if ($company->save()){
                    $company_id = $company->id;
                    foreach ($companyLoop["reviews"] as $revewLoop){
                        $revew = new Revew();
                        $revew->title = $revewLoop["title"];
                        $revew->user = $revewLoop["user"];
                        $revew->company_id = $company_id;
                        if ($revew->save()) {
                            $rating = new Rating();
                            $rating->culture = $revewLoop["rating"]["culture"];
                            $rating->management = $revewLoop["rating"]["management"];
                            $rating->work_live_balance = $revewLoop["rating"]["work_live_balance"];
                            $rating->career_development = $revewLoop["rating"]["career_development"];
                            $rating->revew_id = $revew->id;
                            $rating->save();
                        }

                    }
                }

            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }

        return true;
    }
}
