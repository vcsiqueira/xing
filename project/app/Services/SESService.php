<?php
namespace App\Services;



use App\Models\TechxUtil;
use App\SES\SON_Amazon_SES;
use App\SES\SON_Amazon_SESMessage;


class SESService{

    /*
     * @var SON_Amazon_SESMessage
     */

    protected $msg;

    /*
     * @var SON_Amazon_SES
     */
    protected $ses;
    /**
     * SESProvider constructor.
     */
    public function __construct(SON_Amazon_SESMessage $msg, SON_Amazon_SES $ses)
    {
        $this->msg = $msg;
        $this->ses = $ses;

        //Setting credentials
        $credentials = config('services.ses');
        $this->ses->setAuth($credentials['key'], $credentials['secret']);
    }

    public function sendMail($to, $subject, $message){

//        dd($this->ses->getSendStatistics());
//        dd($this->ses->listVerifiedEmailAddresses());

        $this->msg->setFrom("vinicius@techx.eng.br");
        $this->msg->addTo($to);
        $this->msg->setSubject($subject);
        $this->msg->setMessageFromString($message);

        return $this->ses->sendEmail($this->msg);

    }

}


?>
