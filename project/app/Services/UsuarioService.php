<?php

/**
 * Created by PhpStorm.
 * User: vcsiqueira
 * Date: 14/09/15
 * Time: 14:55
 */

namespace App\Services;

use App\Criterias\UsuarioCriteria;
use App\Models\TechxUtil;
use App\Repositories\UsuarioRepository;
use App\Validators\UsuarioValidator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Prettus\Validator\Exceptions\ValidatorException;
use Ratchet\Wamp\Exception;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class UsuarioService {
    /*
     * @var UsuarioValidator
     */

    protected $validator;

    /*
     * @var UsuarioRepository
     */
    protected $repository;

    protected $userLogged;

    /**
     * UsuarioService constructor.
     */
    public function __construct(UsuarioRepository $repository, UsuarioValidator $validator) {
        $this->repository = $repository;
        $this->validator = $validator;
    }



    public function trocarSenha($userId, $credentials) {

        $usuario = $this->repository->find($userId);

        //Confirmando existência do usuário
        if (empty($usuario))
            throw new \Exception("Usuario não encontrado", 404);

        //Confirmando equivalência
        if ($credentials['senha_nova'] != $credentials['senha_nova_confirmacao'])
            throw new \Exception("Confirmação de senha não se equivale com a nova senha", 404);

        //Confirmando se a senha atual
        if (!Hash::check($credentials['senha_atual'], $usuario->password))
            throw new \Exception("Senha não confere", 404);

        if ($usuario->fill([
                    'password' => $credentials['senha_nova']
                ])->save()
        )
            return TechxUtil::montaRetorno(200, Array('usuario' => $usuario));

        throw new \Exception("Erro ao salvar o usuário", 404);
    }

    public function redefinirSenha($userId) {

        $usuario = $this->repository->find($userId);

        return $usuario->fill([
                    'password' => 12345678
                ])->save();
    }

    public function update(array $data, $id) {

//        dd($data);
        $usuario = $this->repository->find($id);
        if (!$usuario)
            return TechxUtil::montaRetorno(500, Array('status' => "Usuario " . $id . " não encontrado."));


        $allPerm = PermissionAccess::all();
        foreach ($allPerm as $perm) {
            $usuario->permissions()->detach($perm->id);
        }

        if (isset($data["permission_selected"]) && is_array($data["permission_selected"])) {

            foreach ($data["permission_selected"] as $permission) {
                $usuario->permissions()->attach($permission);
            }
        }

        try {
            $this->validator->with($data)->passesOrFail();
            $this->repository->update($data, $id);
            return TechxUtil::montaRetorno(200, Array('usuario' => $usuario));
        } catch (ValidatorException $e) {
            return [
                'error' => true,
                'message' => $e->getMessageBag()
            ];
        }
    }

    public function destroy($id) {
        $usuario = $this->repository->find($id);
        if (!$usuario)
            return TechxUtil::montaRetorno(500, Array('status' => "Usuario " . $id . " não encontrado."));

        if ($usuario->delete())
            return TechxUtil::montaRetorno(200, Array('status' => 'success'));;
    }

    public function create(array $data) {
        try {
            $this->validator->with($data)->passesOrFail();
            return $this->repository->create($data);
        } catch (ValidatorException $e) {
            return TechxUtil::montaRetorno(500, Array('status' => $e->getMessageBag()));
        }
    }

    public function show($loggeduser, $idUser) {

        $usuario = $this->repository->with('empresa')->find($idUser);
        if (!$usuario)
            return TechxUtil::montaRetorno(404, Array('status' => 'not found'));
        if ($loggeduser->empresa_id != $usuario->empresa_id)
            throw new JWTException("permission_denied", 403);
        $permissions = $usuario->permissions()->get(["id"]);
        $returnPerm = array();
        foreach ($permissions as $perm) {
            $returnPerm[] = $perm->id;
        }


        return TechxUtil::montaRetorno(200, Array('usuario' => $usuario, 'permissions' => $returnPerm));
    }

    public function login($credentials) {

        $usuario = $this->repository->findByField('username', $credentials['username'])->first();
//        dd($usuario->getAttributes());

        $customClaims = array();

        try {

            if (!$token = JWTAuth::attempt($credentials, $customClaims)) {
                throw new JWTException("Invalid Credentials", 401);
            }
        } catch (JWTException $e) {
            throw new JWTException($e->getMessage(), $e->getCode());
        }

        $imei = "";

        return TechxUtil::montaRetorno(200, Array('usuario' => $usuario, 'token' => $token, 'imei' => $imei, 'menu' => ['mainMenu' => $this->getMainMenu()]));
    }


    public function index(Request $request) {

        $this->userLogged = JWTAuth::parseToken()->toUser();
        /*
          return $usuario = $this->repository->with(['empresa'])->scopeQuery(function ($query) use ($empresa_id) {

          return $query->join('empresa', 'empresa_id', '=', 'empresa.id')
          ->select("usuario.*")
          ->where(array("empresa.id" => $empresa_id, "empresa.status" => "A"));
          })->all();
         */

//		 DB::connection()->enableQueryLog();
        $pagination = 10;
        $count = $request->get(config('repository.criteria.params.count', 'count'), null);
        if (isset($count) && !empty($count) && is_numeric($count)) {
            $pagination = $count;
        }

//         DB::connection()->enableQueryLog();

        $comCriteria = $this->repository->pushCriteria(new UsuarioCriteria($request))->paginate($pagination)->toArray();

//         dd(DB::getQueryLog());



        return $comCriteria;
    }

    public function getMainMenu() {
        $retorno = [
            'Produtos' => [
                'show' =>  true,
            ],
            'Carrinho' => [
                'show' => true,
            ]
        ];

        return $retorno;
    }





}
