<?php

namespace App\Services;

use App\Models\Company;
use App\Models\Rating;
use App\Models\Revew;
use App\Repositories\CompanyRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class RevewService {

    public function store($company_id, $request) {

        DB::beginTransaction();
        try {
            $revew = new Revew();
            $revew->title = $request["title"];
            $revew->user = $request["user"];
            $revew->company_id = $company_id;
            if ($revew->save()) {
                $rating = new Rating();
                $rating->culture = $request["rating"]["culture"];
                $rating->management = $request["rating"]["management"];
                $rating->work_live_balance = $request["rating"]["work_live_balance"];
                $rating->career_development = $request["rating"]["career_development"];
                $rating->revew_id = $revew->id;
                $rating->save();
            }


            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }

        return true;
    }
}
