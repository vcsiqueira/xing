<?php

namespace App\Validators;
/**
 * Created by PhpStorm.
 * User: vcsiqueira
 * Date: 14/09/15
 * Time: 14:57
 */

use Prettus\Validator\LaravelValidator;

class UsuarioValidator extends LaravelValidator
{
    protected $rules = [
        'empresa_id' => 'required|integer',
        'nome' => 'required',
        'email' => 'required',
        'username' => 'required',
//        'password' => 'required',
        'status' => 'required',
    ];
}