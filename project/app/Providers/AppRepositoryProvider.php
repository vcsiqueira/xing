<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\TechxSegmentoRepository;

class AppRepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \App\Repositories\UsuarioRepository::class,
            \App\Repositories\UsuarioRepositoryEloquent::class
        );

        $this->app->bind(
            \App\Repositories\ProductsRepository::class,
            \App\Repositories\ProductsRepositoryEloquent::class
        );

        $this->app->bind(
            \App\Repositories\OrderRepository::class,
            \App\Repositories\OrderRepositoryEloquent::class
        );

        $this->app->bind(
            \App\Repositories\OrderProductsRepository::class,
            \App\Repositories\OrderProductsRepositoryEloquent::class
        );

        $this->app->bind(
            \App\Repositories\CompanyRepository::class,
            \App\Repositories\CompanyRepositoryEloquent::class
        );

        $this->app->bind(
            \App\Repositories\RevewRepository::class,
            \App\Repositories\RevewRepositoryEloquent::class
        );

        $this->app->bind(
            \App\Repositories\RatingRepository::class,
            \App\Repositories\RatingRepositoryEloquent::class
        );

    }
}
