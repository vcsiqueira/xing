<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Revew extends Model
{

    protected $table = 'revew';
    protected $primaryKey = 'id';
    protected $fillable = array('title', 'user','company_id');
    protected $hidden = array('created_at', 'updated_at');

    public function rating()
    {
        return $this->hasOne(Rating::class, 'revew_id');
    }
}
