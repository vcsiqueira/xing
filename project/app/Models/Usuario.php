<?php

# app/Models/Usuario.php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Auth\Authenticatable;

final class Usuario extends Model implements Authenticatable {

    protected $table = 'usuario';
    protected $fillable = array('nome', 'username', 'password', 'status', 'email');
    protected $hidden = array('password', 'created_at', 'updated_at');

// 	protected $with = array('empresa');
    public function getAuthIdentifier() {
        return $this->id;
    }

    public function setPasswordAttribute($value) {
        $this->attributes['password'] = bcrypt($value);
    }

    public function getAuthPassword() {
        return $this->password;
    }

    public function getRememberToken() {
        die("DIE 1");
    }

    public function setRememberToken($value) {
        die("DIE 2");
    }

    public function getRememberTokenName() {
        die("DIE 3");
    }
}
