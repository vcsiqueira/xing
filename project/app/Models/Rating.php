<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{

    protected $table = 'rating';
    protected $fillable = array('culture', 'management', 'work_live_balance', 'career_development', 'revew_id');
    protected $hidden = array('created_at', 'updated_at');

}
