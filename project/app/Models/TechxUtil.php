<?php

# app/Models/TechxUtil.php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\App;
use Psy\Util\Json;

use Log;

final class TechxUtil {

    public static $paginationPerPage = 10;

    public static function createPaginationQuery(Builder $model, Request $request, $returnModel = false) {
        $arrayReservadas = array("sort-by" => "", "sort-order" => "", "count" => "", "page" => "", "token" => "");
        $per_page = self::$paginationPerPage;

        # ?sort-by=id&sort-order=asc&page=1&count=10&cliente_desc=Cliente
        // 		die(print_r(array_diff_key($request->all(), $arrayReservadas)));



        if (!$returnModel) {
            if ($request->has("sort-by")) {
                $model->orderBy($request->input("sort-by"), $request->input("sort-order"));
            } else {
                $model->orderBy("header-id", "DESC");
            }
        }

        if ($request->has("count") && is_numeric($request->input("count"))) {
            $per_page = $request->input("count");
        }

        foreach (array_diff_key($request->all(), $arrayReservadas) as $campo => $valor) {
            if (is_numeric($valor) && $campo === 'header-id') {
                $model = $model->where(str_replace("-", ".", $campo), "=", $valor);
            } else {
                $model->where(str_replace("-", ".", $campo), "ILIKE", '%' . $valor . '%');
            }
        }

        if ($returnModel) {
            return $model;
        } else {
            return $model->paginate($per_page)->toArray();
        }
    }

	 /**
	 * Grava Log em storage/laravel.log se a variável APP_DEBUG estiver ligado (true)
	 *
	 * @author José Luiz Neto
	 * @since 2016/01/28
	 *
	 * @param String ['debug', 'emergency', 'alert', 'critical', 'error', 'warning', 'notice', 'info'(Default)] $tipo
	 * @param Sring|Array $texto
	 * @return void
	 */
	 public static function log($tipo, $texto, $tag = "") {
		 try {
			 if (strtolower(getenv('APP_DEBUG')) != 'true') { return; }

			$textoOrig = $texto;
			 $bt = debug_backtrace();
			 $caller = array_shift($bt);
			 $file = $caller['file'];
			 $file = explode("\\app\\", $file);
			 if (isset($file[1])) {
				 $file = $file[1];
			 }
			 
			 $info = [
				 "data" => Date("Y-m-d H:i:s"),
				 "file" => $file,
				 "linha" => $caller['line'],
			 ];

			 //  if (is_array($texto) || is_object($texto)) {
			 //  } else {
			 if (empty($tag)) {
			 	$info['texto'] = $textoOrig;
			} else {
				$info[strtoupper($tag)] = $textoOrig;
			}
			 //  }

			 $texto = json_encode($info);#."\n";
			 if (json_last_error() > 0) {
				 $info['error'] = json_last_error_msg();
				 $texto = print_r($info, true);
			 }

			 //$texto = $texto."\n";

			 switch (strtolower($tipo)) {
				 case 'debug':
				 Log::debug($texto);
				 break;
				 case 'emergency':
				 Log::emergency($texto);
				 break;
				 case 'alert':
				 Log::alert($texto);
				 break;
				 case 'critical':
				 Log::critical($texto);
				 break;
				 case 'error':
				 Log::error($texto);
				 break;
				 case 'warning':
				 Log::warning($texto);
				 break;
				 case 'notice':
				 Log::notice($texto);
				 break;
				 case 'info':
				 default:
				 Log::info($texto);
				 break;
			 }
		 } catch (Exception $e) {
			// IGNORA A EXCEPTION POIS É APENAS UM LOG. :D
		 }

	 }

    public static function montaRetorno($code, $objeto) {
        $retorno = array(
            'response' => array(
                'responsecode' => $code
        ));

        $retorno['response'] = array_merge($retorno['response'], $objeto);

        return $retorno;
    }

    public static function arrayAssoc($array) {
        $retorno = array();
        foreach ($array as $key => $value) {
            $retorno = array_merge_recursive($retorno, self::arrayAssocItem($key, $value, true));
        }
        return $retorno;
    }

    public static function arrayAssocItem($itemChave, $value, $primeiro = false) {
        $retorno = array();
        $item = explode(".", $itemChave);
        if (count($item) > 1) {
            $temp = array_shift($item);
            if ($primeiro) {
                $retorno = self::arrayAssocItem(implode(".", $item), $value);
            } else {
                $retorno[$temp] = self::arrayAssocItem(implode(".", $item), $value);
            }
        } else {
            $retorno[$item[0]] = $value;
        }
        return $retorno;
    }

    /**
     * Decodifica o Token.
     *
     * @param  String $token
     * @return mixed Array|boolean
     */
    public static function tokenDecode($token, $returnArray = false) {
        try {
            $parts = explode('.', $token);
            // 			$return = array();
            $return = '';

            $header = str_pad($parts[0], 4 - (strlen($parts[0]) % 4), '=');
            $body = str_pad($parts[1], 4 - (strlen($parts[1]) % 4), '=');
            $signature = str_pad($parts[2], 4 - (strlen($parts[2]) % 4), '=');

            $return.= '{"header":' . base64_decode(strtr($header, '-_', '+/'));
            $return.= ',"body":' . base64_decode(strtr($body, '-_', '+/'));
            // 			$return.= ',"signature":'.base64_decode(strtr($decoded, '-_', '+/'));
            $return.="}";

            // 			$signWith = implode('.', array($header, $body));
            // 			$signature = hash_hmac('SHA256', $signWith, $key, true);
            // 			$return['header'] = json_decode(base64_decode(strtr($header, '-_', '+/')), true);
            // 			$return['body'] = json_decode(base64_decode(strtr($body, '-_', '+/')), true);
            // 			$return['signature'] = json_decode(base64_decode(strtr($signature, '-_', '+/')), true);

            return json_decode($return, $returnArray);
        } catch (Exception $e) {
            return false;
        }
    }

    protected static function getSystemMethodsToAllow($className, $classNameLower)
    {
        $methodNames = [];
        $reflectionClass = new \ReflectionClass('App\Http\Controllers\\' . $className);
        $publicMethods = $reflectionClass->getMethods(\ReflectionMethod::IS_PUBLIC);

        $lowerClassName = strtolower('App\Http\Controllers\\' . $className);
        foreach ($publicMethods as $method) {
			  $comment = $method->getDocComment();
            if (strtolower($method->class) == $lowerClassName) {
					// You can skip this check if you need constructor\destructor
					if (!($method->isConstructor() ||
					$method->isDestructor())) {
						$methodNames[] = '"' . strtolower($method->getName()) . '"' . ':' . 'false';
//							'description' => self::getDocComment($comment, '@description'),
//							'name' => $classNameLower
//						];


					}
            } else {
                // exit loop if class mismatch
                break;
            }
        }
        return $methodNames;
    }

    public static function  getSystemActionsToAllow()
    {
        $dir = App::getFacadeApplication()->basePath() . '/app/Http/Controllers';
        $files = scandir($dir);

        $models = array();
        foreach ($files as $file) {
            //skip current and parent folder entries and non-php files
            if ($file != '.' && $file != '..' && strtolower(str_replace('Controller.php', '', $file)) != "") {
                $class = str_replace('.php', '', $file);
                $methods = self::getSystemMethodsToAllow($class, strtolower(str_replace('Controller.php', '', $file)));
                $models[] = ['name'=> strtolower(str_replace('Controller.php', '', $file)), 'slug' => '{' . implode(',',$methods) . '}'];
            }
        }

        return $models;
    }

    public static function getActionNameNormalizedToPermission($fullname){
        // Example: 'App\\Http\\Controllers\\AclController@index'
        $replaced = str_replace('App\\Http\\Controllers\\', '', $fullname);
        $replaced = str_replace('Controller', '', $replaced);
        $exploded = explode('@', strtolower($replaced));

        return $exploded[1] . '.' . $exploded[0];
    }

	 public static function getDocComment($str, $tag = '')
	 {
	     if (empty($tag))
	     {
	         return $str;
	     }

	     $matches = array();
	     preg_match("/".$tag." (.*)(\\r\\n|\\r|\\n)/U", $str, $matches);

	     if (isset($matches[1]))
	     {
	         return trim($matches[1]);
	     }

	     return '';
	 }


}
