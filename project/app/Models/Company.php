<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{

    protected $table = 'company';
    protected $primaryKey = 'id';
    protected $fillable = array('name', 'slug', 'city', 'country', 'industry');
    protected $hidden = array('created_at', 'updated_at');


    public function revews()
    {
        return $this->hasMany(Revew::class,'company_id');
    }

}
