<?php
/**
 * Created by PhpStorm.
 * User: vcsiqueira
 * Date: 14/09/15
 * Time: 14:11
 */

namespace App\Repositories;


use App\Models\Usuario;
use Prettus\Repository\Eloquent\BaseRepository;

class UsuarioRepositoryEloquent extends BaseRepository implements UsuarioRepository
{

    protected $fieldSearchable = [
        'public.usuario.name',
        'public.usuario.email',
        'public.usuario.status',
        'public.usuario.type_user',
        'public.usuario.roles_medicao',
        'public.usuario.id',
        'public.empresa.descricao',
        'public.workflow.status',
        'public.usuario.master_editor'
    ];

    public function model(){
        return Usuario::class;
    }

}