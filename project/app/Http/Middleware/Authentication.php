<?php

namespace App\Http\Middleware;
use App\Models\TechxUtil;

use Tymon\JWTAuth\Validators\TokenValidator;

use Tymon\JWTAuth\Payload;

use Tymon\JWTAuth\Validators\PayloadValidator;

use Tymon\JWTAuth\Token;

use App\Models\Usuario;

use Tymon\JWTAuth\Exceptions\TokenExpiredException;

use Tymon\JWTAuth\Exceptions\JWTException;

use Tymon\JWTAuth\Facades\JWTAuth;

use Illuminate\Http\Request;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class Authentication
{
	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle(Request $request, Closure $next)
	{
	    try {
			$token = JWTAuth::parseToken();
			$user = $token->toUser();

            $request->setUserResolver(function() use ($user) {
                return $user;
            });
            $actions = $request->route()->getAction();

            $actions['can'] = (isset($actions['can'])) ? $actions['can'] : TechxUtil::getActionNameNormalizedToPermission($actions['uses']);
            $request->route()->setAction($actions);

			if (!$user) {
				throw new JWTException("User not authorized", 401);
			}
		} catch (TokenExpiredException $e) {
			$tokenDecoded = TechxUtil::tokenDecode($token->getToken());
			$usuObj = new Usuario();
			$usuario = $usuObj->find($tokenDecoded->body->sub);
			if ($usuario->created_at->timestamp < $tokenDecoded->body->iat) {
				throw new \Exception(JWTAuth::fromUser($usuario), 401);
			} else {
				throw new JWTException("User not authorized", 401);
			}
		}

		return $next($request);
	}
	

}
