<?php

namespace App\Http\Controllers;

use App\Models\TechxUtil;
use App\Repositories\CompanyRepository;
use App\Services\CompanyService;
use Illuminate\Http\Request;

class CompanyController extends Controller
{


    private $service;
    private $repository;

    /**
     * CompanyController constructor.
     * @param $service
     */
    public function __construct(CompanyService $service, CompanyRepository $repository)
    {
        $this->service = $service;
        $this->repository = $repository;
    }


    /**
     * Load the json file
     */
    public function load()
    {
        return TechxUtil::montaRetorno(200, ["message"=>$this->service->load()]);
    }

    public function index()
    {
        return TechxUtil::montaRetorno(200, ["companies"=>$this->repository->with(["revews","revews.rating"])->all()]);
    }


    //A user can view companies and their average ratings
    public function getCompaniesWithRating()
    {
        return TechxUtil::montaRetorno(200, ["companies"=>$this->repository->with(["revews.rating"])->all()]);
    }


    //A user can view a specific company and read all their reviews
    public function getRevewByCompanyId($company_id)
    {
        return TechxUtil::montaRetorno(200, ["companies"=>$this->repository->with(["revews"])->find($company_id)]);
    }



    /**
    * Write a "users who reviewed this company also reviewed"
    * - function: Given a particular company list other companies that other users also reviewed.
    */

    public function getUsersWhoReviewedTheseCompaniesAlsoReviewThese(Request $request){

        $param = $request->all();

        $compList = $this->repository->with(["revews"])->findWhereIn("id",$param["companies_id"]);

        $users = [];
        foreach ($compList as $comp){
            if (ISSET($comp["revews"]) && count($comp["revews"]) > 0){
                foreach ($comp["revews"] as $review){
                    $users[] = $review["user"];
                }
            }
        }

//        DB::connection()->enableQueryLog();
        $companies = $param["companies_id"];
        $return = $this->repository->with(["revews"])->scopeQuery(function ($query) use ($users, $companies) {
            return $query->join('revew', 'company_id', '=', 'company.id')
                ->whereIn("revew.user",$users)
                  ->whereNotIn("company.id",$companies);
        })->all();

//        dd(DB::getQueryLog());
        return TechxUtil::montaRetorno(200, ["companies"=>$return]);

    }

}
