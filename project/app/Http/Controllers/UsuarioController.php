<?php

namespace App\Http\Controllers;

use App\Models\TechxUtil;
use App\Models\Usuario;
use App\Repositories\UsuarioRepository;
use App\Services\UsuarioService;
use App\Services\SESService;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class UsuarioController extends Controller {
    /*
     * @var UsuarioRepository
     */

    protected $repository;

    /*
     * @var UsuarioService
     */
    protected $service;

    /*
     * @var SESService
     */
    protected $ses;

    /**
     * UsuarioController constructor.
     */
    public function __construct(UsuarioService $service, UsuarioRepository $repository, SESService $ses) {
        $this->repository = $repository;
        $this->service = $service;
        $this->ses = $ses;
    }

    /**
     *
     *
     * @param Request $request
     * @return type
     */
    public function permissaoFormulario(Request $request) {
        return $this->service->updatePermissaoFormulario($request);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function comboFiscal() {
        return TechxUtil::montaRetorno(200, Array('fiscal' => $this->service->getComboFiscal()));
    }

    public function login(Request $request) {
        return $this->service->login($request->all());
    }


    public function index(Request $request) {
        return TechxUtil::montaRetorno(200, Array('usuario' => $this->service->index($request)));
    }

    public function show($id) {
        return $this->service->show($this->getMe(), $id);
    }

    public function store(Request $request) {
        return $this->service->create($request->all());
    }

    public function saveNewUsuarioGcm(Request $request) {
        return $this->service->newUsuarioGcm($request->all());
    }

    public function sendMessageUsuarioGcm(Request $request) {
        return $this->service->sendMessageUsuarioGcm($request->all());
    }

    public function destroy($id) {
        return $this->service->destroy($id);
    }

    public function update(Request $request, $id) {
        return $this->service->update($request->all(), $id);
    }

    public function trocarsenha(Request $request, $id) {
        return $this->service->trocarSenha($id, $request->all());
    }

    public function resetSenha($id) {
        if ($this->service->redefinirSenha($id)) {
            return TechxUtil::montaRetorno(200, Array('status' => "Nova senha: 12345678!"));
        }

        return TechxUtil::montaRetorno(500, Array('status' => 'Erro ao redefinir a senha'));
    }

    protected function getMe() {
        return JWTAuth::parseToken()->toUser();
    }

    public function redefinirSenha(Request $request) {
        $appName = config('app.appName');
        $email = $request->only('email');
        $user = $this->repository->findByField("email", $email['email'])->first();

        if (!isset($user))
            throw new HttpException(404, "E-mail não encontrado !");

        $token = JWTAuth::fromUser($user);
        $request->headers->set("authorization", "Bearer $token");

        if ($this->service->redefinirSenha($user->id)) {
            $this->ses->sendMail($user->email, $appName . ' - Redefinição de senha', "Nova senha: 12345678");
            return TechxUtil::montaRetorno(200, Array('status' => "Senha enviada para seu e-mail !"));
        }

        return TechxUtil::montaRetorno(404, Array('status' => 'Erro ao redefinir a senha'));
    }

    public function getUsuarioRole($id) {

        $user = new Usuario();
        return $user->getUsuarioRole($id);
    }

    public function teste() {
        return $this->service->getMenuWorkflow(2);
    }

    public function gcmGetMessages() {
        return $this->service->getGcmNotifications();
    }

    public function checkMasterEditor($id) {

        $master_editor = $this->repository->find($id, array("master_editor"));

        return TechxUtil::montaRetorno(200, Array('data' => $master_editor));
    }

}
