<?php

namespace App\Http\Controllers;

use App\Models\TechxUtil;
use App\Services\RevewService;
use Illuminate\Http\Request;

class RevewController extends Controller
{


    private $service;

    public function __construct(RevewService $service)
    {
        $this->service = $service;
    }


    //A user can submit a review for a company
    public function store($company_id, Request $request)
    {
        return TechxUtil::montaRetorno(200, ["Company_id"=>$company_id, "result"=>$this->service->store($company_id, $request->all())]);
    }
}
