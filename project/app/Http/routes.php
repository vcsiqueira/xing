<?php

Session::flush();

Route::get(App::environment() . '/teste', function(){
    return 'teste';
});

$environment = App::environment();

//loading
Route::get($environment . '/company/seed','CompanyController@load');

Route::get($environment . '/company','CompanyController@index');

//A user can submit a review for a company
Route::post($environment . '/revew/{company_id}','RevewController@store');

//A user can view companies and their average ratings
Route::get($environment . '/company/rating','CompanyController@getCompaniesWithRating');

//A user can view a specific company and read all their reviews
Route::get($environment . '/company/{company_id}/revew','CompanyController@getRevewByCompanyId');

/* Write a "users who reviewed this company also reviewed"
* - function: Given a particular company list other companies that other users also reviewed.
*/
Route::post($environment . '/company/alsoReviewed','CompanyController@getUsersWhoReviewedTheseCompaniesAlsoReviewThese');