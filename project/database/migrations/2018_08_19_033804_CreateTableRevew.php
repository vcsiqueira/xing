<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRevew extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revew', function (Blueprint $table) {
            $table->increments('id');
            $table->string("title");
            $table->string("user");
            $table->integer("company_id");

            $table->foreign("company_id")->references('id')->on('company');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('revew');
    }
}
