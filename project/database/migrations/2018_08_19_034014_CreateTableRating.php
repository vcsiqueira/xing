<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRating extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rating', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("culture");
            $table->integer("management");
            $table->integer("work_live_balance");
            $table->integer("career_development");
            $table->integer("revew_id");
            $table->timestamps();

            $table->foreign("revew_id")->references('id')->on('revew');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rating');
    }
}
