<?php

use Illuminate\Database\Seeder;

class CreateUsuario2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = \Illuminate\Support\Facades\Storage::disk('local')->get('data.json');
        $data = json_decode($json,true);

        DB::beginTransaction();
        try {
            //companies
            foreach ($data["companies"] as $companyLoop){
                $company = new Company();
                $company->name = $companyLoop["name"];
                $company->slug = $companyLoop["slug"];
                $company->city = $companyLoop["city"];
                $company->country = $companyLoop["country"];
                $company->industry = $companyLoop["industry"];

                if ($company->save()){
                    $company_id = $company->id;
                    foreach ($companyLoop["reviews"] as $revewLoop){
                        $revew = new Revew();
                        $revew->title = $revewLoop["title"];
                        $revew->user = $revewLoop["user"];
                        $revew->company_id = $company_id;
                        if ($revew->save()) {
                            $rating = new Rating();
                            $rating->culture = $revewLoop["rating"]["culture"];
                            $rating->management = $revewLoop["rating"]["management"];
                            $rating->work_live_balance = $revewLoop["rating"]["work_live_balance"];
                            $rating->career_development = $revewLoop["rating"]["career_development"];
                            $rating->revew_id = $revew->id;
                            $rating->save();
                        }

                    }
                }

            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
