#!/bin/bash

# PHPUNIT_FILTER="$1"

clear
echo "Iniciando os testes..."
echo "Rodando o Migration para os testes (DB=fiscalsherlock-test)..."
php artisan migrate --env=testing

echo "Executando os testes..."
if [ -z "$1" ]
  then
    #php ./phpunit.phar --colors -v --tap --coverage-html=storage/testes
	 php ./phpunit.phar --colors -v --tap
fi

if [ -n "$1" ]
  then
    php ./phpunit.phar --colors --filter= $1
fi

echo "Testes realizados.. coverage em storage/testes"
