
Answer for Test Application

Hello everyone !

First of all, thank you so much for the opportunity !

I tried to challenge my self respecting the time of 4 days in order to be be ready enough to work with Docker, cause as I said on the interview I've never worked with Docker before.
And unfortunately, I didn't get use Symfony, as it was required, because I've never worked with this framework, and I tried to use the Symfony 4, but I had lot of errors of installation and so.
Then I took the decision to work with the Laravel Framework, which is similar but was faster for me because I use daily in my current position.


So let's start ...

In order to up all environment, please run these commands bellow:



First: Get into the folder xing: cd xing

Second: docker-compose up -d

Third: docker container exec -it xing_backend_1 bash -c 'php ../artisan migrate ; curl http://localhost/company/seed'

Follow file called Xing.postamn_collection.json, to load at Postman and run the APi's

//****//

Questions
Briefly try to answer the following:

How much test-coverage is desirable?
In my option around 80%. Although, unfortunately I didn't police my self to do.
It' wrong I confess, but I need be among a company which breathe this culture where I can see how good is TDD and test in general.
By the book, I know that's necessary, but I need live in practice.

What parts of your example do you like the least?
The most difficult was try be ready to do the example in Symfony 4, once I've never worked before with this framework. Then, I used Laravel, cause as I could see the principles seems to be closer.

How would you describe your coding style? What makes your code clean? Can you point out an example?
I think it's clean enough. As you can see, the controllers are so clean, only calling the service or if is only one db transaction calling the repository.

Did you use any design patterns? If so, why did you decide on that particular pattern? If not, then why not?
I confess that I'm not so good with patterns, and I didn't percept any application for theses situations.

How maintainable is your code? What makes it maintainable?
I always try keep my code readable, as you can see my methods are called almost with the same name of the issue, just in order to keep connected and surely readable

What would be your preferred storage for solving a problem like this in a production environment and why?
Although the json file was almost ready to persist in a mongodb for example, I preferred to use Postgres because the last problem was easier to do using SQL.

//*****//